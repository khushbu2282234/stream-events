<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Followers api calls to get and set data
Route::get('/followers',[App\Http\Controllers\FollowersController::class,'index']);
Route::get('followers/{id}', [App\Http\Controllers\FollowersController::class,'show']);
Route::get('followersbyuser/{user_id}', [App\Http\Controllers\FollowersController::class,'followers_by_user_id']);
Route::put('followers_read_unread/{id}', [App\Http\Controllers\FollowersController::class,'followers_update']);


//Merchant sales api calls to get and set data
Route::get('/merchsales',[App\Http\Controllers\MerchsalesController::class,'index']);
Route::get('merchsales/{id}', [App\Http\Controllers\MerchsalesController::class,'show']);
Route::get('merchsalesbyuser/{user_id}', [App\Http\Controllers\MerchsalesController::class,'merchsles_by_user_id']);
Route::put('merchsales_read_unread/{id}', [App\Http\Controllers\MerchsalesController::class,'merchsales_update']);


//Subscribers api calls to get and set data
Route::get('/subscribers',[App\Http\Controllers\SubscribersController::class,'index']);
Route::get('subscribers/{id}', [App\Http\Controllers\SubscribersController::class,'show']);
Route::get('subscribersbyuser/{user_id}', [App\Http\Controllers\SubscribersController::class,'subscribers_by_user_id']);
Route::put('subscribers_read_unread/{id}', [App\Http\Controllers\SubscribersController::class,'subscribers_update']);

//Donations api calls to get and set data
Route::get('/donations',[App\Http\Controllers\DonationsController::class,'index']);
Route::get('donations/{id}', [App\Http\Controllers\DonationsController::class,'show']);
Route::get('donationsbyuser/{user_id}', [App\Http\Controllers\DonationsController::class,'donations_by_user_id']);
Route::put('donations_read_unread/{id}', [App\Http\Controllers\DonationsController::class,'donations_update']);

//route get stream data of followers, subscribers, donations and merchant sales based on logged in user 
Route::get('/getuserstreaminfo/{id}',[App\Http\Controllers\APIController::class,'get_stream_data']);
Route::post('/updatestreamdata',[App\Http\Controllers\APIController::class,'update_read_unread_data']);
