<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Followers>
 */
class FollowersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $createdDate = fake()->dateTimeBetween('-3 months', 'now');
        return [
            'name' => fake()->name(),
            'user_id' =>fake()->numberBetween(1,5),
            'created_at'=>$createdDate ,
            'updated_at'=>$createdDate, 
        ];
    }
}
