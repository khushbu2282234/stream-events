<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Donations>
 */
class DonationsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $createdDate = fake()->dateTimeBetween('-3 months', 'now');
        return [
            'amount' => fake()->randomDigit(),
            'user_id' =>fake()->numberBetween(1,5),
            'donor_name' => fake()->name(),
            'donation_message' =>fake()->text(),
            'currency' => fake()->currencyCode(),
            'created_at'=>$createdDate ,
            'updated_at'=>$createdDate, 
        ];
    }
}
