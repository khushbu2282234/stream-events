<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Merchsales>
 */
class MerchsalesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $createdDate = fake()->dateTimeBetween('-3 months', 'now');
        return [
            'item_name' => fake()->word(),
            'amount' => fake()->randomFloat(2, 10, 1000),
            'price' => fake()->randomFloat(2, 1, 500),
            'buyer_name' => fake()->name(),
            'user_id' =>fake()->numberBetween(1,5),
            'created_at'=>$createdDate ,
            'updated_at'=>$createdDate, 
        ];
    }
}
