@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}

                    <ul id="stream_data"></ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Add jQuery for  ajax request to get data from REST API call  -->
<script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var user_id = <?php echo Auth::id(); ?>;
    $.ajax({
        url: "<?php echo url('/api/getuserstreaminfo/'); ?>/"+user_id,
        type: 'GET',
        dataType: 'json', // added data type
        success: function(res) {
           
            //Prepare list of stream data
            if(res)
            {
                var li_html = "";
                $.each(res, function( index, value ) {
            
                if (index.indexOf('followers_data_') > -1)
                {
                    li_html += "<li class = 'followers_li' style='margin-bottom:2%;' ><a href='#' class ='data_update' data-type='followers' data-id='"+value.id+"'>"+value.content+"</a></li>";
                }
                else if (index.indexOf('subscribers_data_') > -1)
                {
                    li_html += "<li  class = 'subscribers_li'  ><a style='color:#9f00ff; margin-bottom:2%;' href='#' class ='data_update' data-type='subscribers' data-id='"+value.id+"'>"+value.content+"</a></li>";
                }
                else if (index.indexOf('donations_data_') > -1)
                {
                    li_html += "<li  class = 'donations_li' ><a  style='color:grey;margin-bottom:2%;' href='#' class ='data_update' data-type='donations' data-id='"+value.id+"'>"+value.content+value.message+"</a></li>";
                }
                else 
                {
                    li_html += "<li  class = 'merch_sales_li' style='margin-bottom:2%;'><a style='color:#18cf4f; margin-bottom:1%;' href='#' class =' data_update' data-type='merch_sales' data-id='"+value.id+"'>"+value.content+"</a></li>";
                }
                
                });
            }

            // set list data in UL element.
            $("#stream_data").html(li_html);
        }
    });

    //update event based on link click
    $("#stream_data").on('click','a', function(event){
    event.preventDefault();
    //based on type send event for update
    var ele = $(this);
    var id = $(this).attr("data-id");
    var type = $(this).attr("data-type")
    $.ajax({
        url: "<?php echo url('/api/updatestreamdata/'); ?>/",
        type: 'POST',
        dataType: 'json', // added data type
        data: { id: id, type:type,user_id:user_id },
        success: function(res) {
            if(res)
            {
                if(res == 1)
                {
                    ele.css('font-weight', 'bold');
                }
                else
                {
                    ele.css('font-weight', 'normal');
                }
            }
            

        }
    });

    });

});
</script>
