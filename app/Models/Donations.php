<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donations extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
        'user_id',
        'currency',
        'donation_message',
        'is_read',
        'donor_name'
    ];
}
