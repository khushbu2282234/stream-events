<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merchsales extends Model
{
    use HasFactory;

    protected $table = 'merch_sales';
    
    protected $fillable = [
        'item_name',
        'user_id',
        'amount',
        'price',
        'read_by',
        'buyer_name'
    ];
}
