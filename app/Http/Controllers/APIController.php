<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Followers;
use App\Models\Donations;
use App\Models\Subscribers;
use App\Models\Merchsales;

class APIController extends Controller
{
    // function to get stream data based on logged in user
    function get_stream_data($user_id)
    {
        
        //get data of followrs by user id
        $followers = Followers::where('user_id', $user_id)->get()->toArray();

        //get data of subscribers by user id
        $subscribers = Subscribers::where('user_id', $user_id)->get()->toArray();

        //get data of donaitions by user id
        $donations = Donations::where('user_id', $user_id)->get()->toArray();

        //get data of merchant sales by user id
        $merch_sales = Merchsales::where('user_id', $user_id)->get()->toArray();

        //prepare an array to show all listing one by one in sequence followed by followers, subscribers, donations and merchant sales
        $mergedArray = [];
        $length = max(count($followers), count($subscribers),count($donations));

        for ($i = 0; $i < $length; $i++) {
            
            if (isset($followers[$i])) {
                $mergedArray['followers_data_'.$i] = ["id"=>$followers[$i]['id'],"content"=>$followers[$i]['name']. " followed you!"];
            }

            if (isset($subscribers[$i])) {
                $mergedArray['subscribers_data_'.$i] = ["id"=>$subscribers[$i]['id'],"content"=>$subscribers[$i]['name']. " subscribed to you!"];
            }

            if (isset($donations[$i])) {
                $mesg='"'.$donations[$i]['donation_message'].'"';
                $mergedArray['donations_data_'.$i] = ["id"=>$donations[$i]['id'],"content"=>$donations[$i]['donor_name']." donated ".$donations[$i]['amount']."  ".$donations[$i]['currency']." to you! </br>","message"=>$mesg];
            }

            if (isset($merch_sales[$i])) {
                $mergedArray['merch_sales_data_'.$i] = ["id"=>$merch_sales[$i]['id'],"content"=>$merch_sales[$i]['buyer_name']. "  bought some ".$merch_sales[$i]['item_name']." from you for ".$merch_sales[$i]['price']." USD!"];
            }
        }
      
        // send formatted array as jason in response of ajax request
        return response()->json($mergedArray, 200);
    }

    //function to update read and unread data based on click event
    function update_read_unread_data(Request $request)
    {
       $user_id = $request->user_id;
       $id = $request->id;
       $type = $request->type;

        //check if type is followers then update followers is_read column
        if($type == "followers")
        {
            $followers = Followers::find($id);
            if($followers)
            {
                $followers->is_read = ($followers->is_read == 0 ? 1:0);
                $followers->save();
                return $followers->is_read;
            }
        }

        //check if type is subscribers then update subscribers is_read column
        if($type == "subscribers")
        {
            $subscribers = Subscribers::find($id);
            if($subscribers)
            {
                $subscribers->is_read = ($subscribers->is_read == 0 ? 1:0);
                $subscribers->save();
                return $subscribers->is_read;
            }
        }

        //check if type is donations then update donations is_read column
        if($type == "donations")
        {
            $donations = Donations::find($id);
            if($donations)
            {
                $donations->is_read = ($donations->is_read == 0 ? 1:0);
                $donations->save();
                return $donations->is_read;
            }
        }

        //check if type is merch_sales then update merch_sales is_read column
        if($type == "merch_sales")
        {
            $merch_sales = Merchsales::find($id);
            if($merch_sales)
            {
                $merch_sales->is_read = ($merch_sales->is_read == 0 ? 1:0);
                $merch_sales->save();
                return $merch_sales->is_read;
            }
        }
    }
}
