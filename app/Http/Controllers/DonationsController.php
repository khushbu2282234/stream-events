<?php

namespace App\Http\Controllers;

use App\Models\Donations;
use Illuminate\Http\Request;

class DonationsController extends Controller
{
    /**
     * Display a listing of the all donations.
     */
    public function index()
    {
        return Donations::all();
    }

    
    /**
     * get Donations details by user_id
    */
    public function donations_by_user_id($user_id)
    {
        return Donations::where('user_id', $user_id)->get();
    }
   
    /**
     * Donations update is_read while click on link
    */
    public function donations_update(Request $request,$id)
    {
        $donations = Donations::find($id);
        $donations->read_by = ($donations->read_by == 0 ? 1:0);
        $donations->save();
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     *  get Donations details by id
     */
    public function show(Donations $donations)
    {
        return Donations::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Donations $donations)
    {
        //
    }

    /**
     * Donations update is_read while click on link
     */
    public function update(Request $request, Donations $donations)
    {
        $donations = Donations::find($id);
        $donations->read_by = ($donations->read_by == 0 ? 1:0);
        $donations->save();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Donations $donations)
    {
        //
    }
}
