<?php

namespace App\Http\Controllers;

use App\Models\Merchsales;
use Illuminate\Http\Request;

class MerchsalesController extends Controller
{
    /**
     * Display a listing of the all merhcant sales.
     */
    public function index()
    {
        return Merchsales::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * get merchant sales details by id
     */
    public function show(Merchsales $merchsales)
    {
        return Merchsales::find($id);
    }

    /**
     * get merchant sales details by user_id
     */
    public function merchasles_by_user_id($user_id)
    {
        return Merchsales::where('user_id', $user_id)->get();
    }

    /**
     * Merch_sales update is_read while click on link
     */
    public function merchsales_update(Request $request,$id)
    {
        $merch_sales = Merchsales::find($id);
        $merch_sales->read_by = ($merch_sales->read_by == 0 ? 1:0);
        $merch_sales->save();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Merchsales $merchsales)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Merchsales $merchsales)
    {
        $merch_sales = Merchsales::find($id);
        $merch_sales->read_by = ($merch_sales->read_by == 0 ? 1:0);
        $merch_sales->save();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Merchsales $merchsales)
    {
        //
    }
}
