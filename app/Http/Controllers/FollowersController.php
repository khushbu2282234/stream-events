<?php

namespace App\Http\Controllers;

use App\Models\Followers;
use Illuminate\Http\Request;

class FollowersController extends Controller
{
    /**
     * Display a listing of the all followers.
     */
    public function index()
    {
        return Followers::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return Followers::find($id);
    }

    /**
     * get Followers details by user_id
    */
    public function followers_by_user_id($user_id)
    {
        return Followers::where('user_id', $user_id)->get();
    }

    /**
     * Followers update is_read while click on link
     */
    public function followers_update(Request $request,$id)
    {
        //
        $followers = Followers::find($id);
        if($followers)
        {
            $followers->read_by = ($followers->read_by == 0 ? 1:0);
            $followers->save();
        }
        
    }

    /**
     * get Followers details by id
     */
    public function show(Followers $followers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Followers $followers)
    {
        //
    }

    /**
     * Followers update is_read while click on link
     */
    public function update(Request $request, Followers $followers)
    {
        $followers = Followers::find($id);
        $followers->read_by = ($followers->read_by == 0 ? 1:0);
        $followers->save();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Followers $followers)
    {
        //
    }
}
