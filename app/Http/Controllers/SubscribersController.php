<?php

namespace App\Http\Controllers;

use App\Models\Subscribers;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{
    /**
     * Display a listing of the all subscribers.
     */
    public function index()
    {
        return Subscribers::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * get Subscribers details by id
     */
    public function show(Subscribers $subscribers)
    {
        return Subscribers::find($id);
    }

    /**
     * get Subscribers details by user_id
    */
    public function subscribers_by_user_id($user_id)
    {
        return Subscribers::where('user_id', $user_id)->get();
    }
  
    /**
     * Subscribers update is_read while click on link
     */
    public function subscribers_update(Request $request,$id)
    {
        //
        $subscribers = Subscribers::find($id);
        
        $subscribers->read_by = ($subscribers->read_by == 0 ? 1:0);
        $subscribers->save();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Subscribers $subscribers)
    {
        //
    }

    /**
     * Subscribers update is_read while click on link
     */
    public function update(Request $request, Subscribers $subscribers)
    {
        $subscribers = Subscribers::find($id);
        $subscribers->read_by = ($subscribers->read_by == 0 ? 1:0);
        $subscribers->save();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Subscribers $subscribers)
    {
        //
    }
}
